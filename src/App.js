import './App.css'
import {Navbar, Nav ,Container , Button , Form , NavDropdown , Card} from "react-bootstrap";

function App() {

  return (
   <>
       <Navbar bg="light" expand="lg">
           <Container fluid>
               <Navbar.Brand href="#">ASPAN</Navbar.Brand>
               <Navbar.Toggle aria-controls="navbarScroll" />
               <Navbar.Collapse id="navbarScroll">
                   <Nav
                       className="me-auto my-2 my-lg-0"
                       style={{ maxHeight: '100px' }}
                       navbarScroll
                   >
                       <Nav.Link href="#action1">Домой</Nav.Link>
                       <Nav.Link href="#action2">Link</Nav.Link>
                       <NavDropdown title="Link" id="navbarScrollingDropdown">
                           <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
                           <NavDropdown.Item href="#action4">
                               Another action
                           </NavDropdown.Item>
                           <NavDropdown.Divider />
                           <NavDropdown.Item href="#action5">
                               Something else here
                           </NavDropdown.Item>
                       </NavDropdown>
                       <Nav.Link href="#" disabled>
                           Поделиться
                       </Nav.Link>
                   </Nav>
                   <Form className="d-flex">
                       <Form.Control
                           type="search"
                           placeholder="Напишите что нибудь"
                           className="me-2"
                           aria-label="Search"
                       />
                       <Button variant="outline-success">Искать</Button>
                   </Form>
               </Navbar.Collapse>
           </Container>
       </Navbar>

       <Card style={{ width: '18rem' }}>
           <Card.Img variant="top" src="https://oknabishkek.kg/wp-content/uploads/2022/08/FOTO-5-Plastikovoe-okno-kruglojj-formy.jpg" />
           <Card.Body>
               <Card.Title>Card Title</Card.Title>
               <Card.Text>
                   Some quick example text to build on the card title and make up the
                   bulk of the card's content.
               </Card.Text>
               <Button variant="primary">Go somewhere</Button>
           </Card.Body>
       </Card>
       <h1>Тепло и уют <br/>в вашем доме</h1>

    </>
  );
}

export default App;
